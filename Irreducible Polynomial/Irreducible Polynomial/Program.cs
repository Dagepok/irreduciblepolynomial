﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irreducible_Polynomial
{
    internal class Program
    {

        private static void Main(string[] args)
        {
            var finder = new PolynomFinder();
            finder.DoSomething();
        }
    }
}
