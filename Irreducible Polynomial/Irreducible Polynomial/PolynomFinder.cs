﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace Irreducible_Polynomial
{
    public class PolynomFinder
    {
        public int Count { get; set; }
        public List<HashSet<int[]>> Polynomials { get; set; }
        public int N { get; set; }
        public int DegreeCalculated { get; set; }
        public PolynomFinder()
        {
            Polynomials = new List<HashSet<int[]>>();
        }

        public void DoSomething()
        {
            var timer = new Stopwatch();
            timer.Start();
            N = int.Parse(Console.ReadLine());
            Polynomials.Add(new HashSet<int[]>());
            for (var i = 1; i < N; i++)
                if (File.Exists(@"Polynomials\" + i + ".txt"))
                {
                    var polynomials = File.ReadAllLines(@"Polynomials\" + i + ".txt");
                    Polynomials.Add(new HashSet<int[]>());
                    foreach (var polynomial in polynomials)
                    {
                        Polynomials[i].Add(polynomial.Select(x => int.Parse(x.ToString())).ToArray());
                    }
                }
                else
                {
                    DegreeCalculated = i;
                    break;
                }
            if (DegreeCalculated == 0) return;

            for (var i = DegreeCalculated > 0 ? DegreeCalculated : 1; i <= N; i++)
            {
                var polynomial = new int[i + 1];
                polynomial[0] = 1;
                polynomial[polynomial.Length - 1] = 1;
                Polynomials.Add(new HashSet<int[]>());
                FindPolinomials(polynomial, 1);
                var str = new StringBuilder();
                foreach (var irreducible in Polynomials[polynomial.Length - 1])
                    str.Append(string.Join("", irreducible) + Environment.NewLine);
                File.WriteAllText(@"Polynomials\" + i + ".txt", str.ToString());
                Console.WriteLine("Degree " + i + " calculated at " + DateTime.Now + " in "
                    + timer.Elapsed + ". " + Polynomials[i].Count + " irreducible polynomials was found");
                timer.Restart();
            }
        }

        private void CheckIrreducibility(int[] polynomial)
        {
            for (var i = 1; i <= (polynomial.Length - 1) / 2; i++)
            {
                foreach (var irreducible in Polynomials[i])
                {
                    PolynomialArithmetics.Deconv(polynomial, irreducible, out int[] quotient, out int[] remainder);
                    if (remainder.Length == 0) return;
                    //if (PolynomialArithmetics.IsDeconvWithoutRemainder(polynomial, irreducible))
                    //    return;
                }
            }
            lock (Polynomials)
                Polynomials[polynomial.Length - 1].Add(polynomial);
        }


        public void FindPolinomials(int[] polynomial, int position)
        {

            Count++;
            if (position == polynomial.Length - 1)
            {
                if (polynomial.Where(x => x == 1).Count() % 2 != 1) return;
                var a = new Thread(() => CheckIrreducibility((int[])polynomial.Clone())) { IsBackground = false };
                a.Start();
                a.Join();
                return;
            }

            for (var i = 0; i < 2; i++)
            {
                polynomial[position] = i;
                FindPolinomials(polynomial, position + 1);
            }

        }
    }
}