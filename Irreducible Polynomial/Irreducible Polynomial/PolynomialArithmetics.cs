﻿using System;
using System.Linq;

namespace Irreducible_Polynomial
{
    public class PolynomialArithmetics
    {
        ///

        /// Метод деления многочлена на многочлен
        /// 
        /// Коэффициенты многочлена делимого, индекс в массиве - степень элемента многочлена
        /// Коэффициенты многочлена делителя, индекс в массиве - степень элемента многочлена
        /// Коэффициенты многочлена частного, индекс в массиве - степень элемента многочлена
        /// Коэффициенты многочлена остатка, индекс в массиве - степень элемента многочлена
        public static void Deconv(int[] dividend, int[] divisor, out int[] quotient, out int[] remainder)
        {
            quotient = new int[1];
            if (dividend.Length < divisor.Length)
            {
                remainder = (int[])dividend.Clone();
                return;
            }
            if (dividend[dividend.Length - 1] == 0)
                throw new ArithmeticException("Старший член многочлена делимого не может быть 0");

            if (divisor[divisor.Length - 1] == 0)
                throw new ArithmeticException("Старший член многочлена делителя не может быть 0");


            var thisRemainder = (int[])dividend.Clone();
            //  var thisQuotient = new int[thisRemainder.Length - divisor.Length + 1];
            for (var i = 0; i < dividend.Length - divisor.Length + 1; i++)
            {
                var coeff = thisRemainder[thisRemainder.Length - i - 1] / divisor[divisor.Length - 1] % 2;
                //      thisQuotient[thisQuotient.Length - i - 1] = coeff;
                for (var j = 0; j < divisor.Length; j++)
                {
                    thisRemainder[thisRemainder.Length - i - j - 1] -= coeff * divisor[divisor.Length - j - 1];
                    thisRemainder[thisRemainder.Length - i - j - 1] = Math.Abs(thisRemainder[thisRemainder.Length - i - j - 1] % 2);
                }
            }
            remainder = thisRemainder.Reverse().SkipWhile(x => x == 0).Reverse().ToArray();
            //   quotient = thisQuotient.Reverse().SkipWhile(x => x == 0).Reverse().ToArray();
        }

        public static int[] Gcd(int[] a, int[] b)
        {
            Deconv(a, b, out int[] quotient, out int[] divisor);
            if (divisor.All(x => x == 0)) return quotient;
            var previousDivisor = (int[])divisor.Clone();
            a = b;
            b = divisor;
            while (true)
            {
                {
                    Deconv(a, b, out quotient, out divisor);
                    if (divisor.All(x => x == 0))
                        break;
                    previousDivisor = (int[])divisor.Clone();
                    a = b;
                    b = divisor;
                }
            }
            return previousDivisor;
        }

        public static int[] Mod(int[] a, int[] b)
        {
            Deconv(a, b, out int[] quotient, out int[] divisor);
            return divisor;
        }

        //public static bool IsDeconvWithoutRemainder(int[] divident, int[] divisor)
        //{
        //    var p = divident.Skip(1).Take(divident.Length - divisor.Length - 1).TakeWhile(x => x == 0).Count();

        //    return divident.Length - p < divisor.Length;

        //}
    }
}